package tokyo.northside.dict;

/**
 * Dict URL protocol handler installer.
 * <p>
 *     You can install data protocol handler by calling
 *     <pre>DictProtocolHandler.install();</pre>
 *
 * @author Hiroshi Miura
 */
public class DictProtocolHandler {

    private static final String PKG =  "tokyo.northside.dict";
    private static final String CONTENT_PATH_PROP = "java.protocol.handler.pkgs";

    private DictProtocolHandler() {
    }

    public static void install() {
        String handlerPkgs = System.getProperty(CONTENT_PATH_PROP, "");
        if (!handlerPkgs.contains(PKG)) {
            if (handlerPkgs.isEmpty()) {
                handlerPkgs = PKG;
            } else {
                handlerPkgs += "|" + PKG;
            }
            System.setProperty(CONTENT_PATH_PROP, handlerPkgs);
        }
    }
}
